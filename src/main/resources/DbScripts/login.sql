DROP TABLE IF EXISTS auth_mgmt.user_role;
DROP TABLE IF EXISTS auth_mgmt.user;

CREATE TABLE library_mgmt.users (
    user_id int NOT NULL AUTO_INCREMENT,
    username varchar(15) NOT NULL,
    email varchar(40) NOT NULL,
    password varchar(100) NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    CONSTRAINT library_users_user_id_PK PRIMARY KEY (user_id),
    CONSTRAINT library_users_username_unique UNIQUE (username),
    CONSTRAINT library_users_email_unique UNIQUE (email)
);

CREATE TABLE library_mgmt.roles (
    role_id int NOT NULL AUTO_INCREMENT,
    name varchar(60) NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    CONSTRAINT library_roles_id_PK PRIMARY KEY (role_id),
    CONSTRAINT library_roles_name_unique UNIQUE (name)
);

INSERT INTO library_mgmt.roles(name) VALUES('ROLE_USER');
INSERT INTO library_mgmt.roles(name) VALUES('ROLE_ADMIN');
INSERT INTO library_mgmt.roles(name) VALUES('ROLE_STUDENT');
INSERT INTO library_mgmt.roles(name) VALUES('ROLE_STAFF');

CREATE TABLE library_mgmt.user_roles (
    user_id int,
    role_id int,
    created_at TIMESTAMP DEFAULT NOW(),
    updated_at TIMESTAMP DEFAULT NOW(),
    CONSTRAINT user_roles_user_id_pk PRIMARY KEY (user_id,role_id),
    CONSTRAINT fk_user_roles_role_id FOREIGN KEY (role_id) REFERENCES library_mgmt.roles (role_id),
    CONSTRAINT fk_user_roles_user_id FOREIGN KEY (user_id) REFERENCES library_mgmt.users (user_id)
);

commit;
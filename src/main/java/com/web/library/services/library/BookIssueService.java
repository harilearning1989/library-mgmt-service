package com.web.library.services.library;

import com.web.library.dtos.IssueBookDto;
import com.web.library.dtos.IssuedBookStudentDto;
import com.web.library.models.library.IssueBook;

import java.util.List;

public interface BookIssueService {
    List<IssueBookDto> findAllIssuedBooks();

    List<IssuedBookStudentDto> findIssuedBooksForStudent(int studentId);

    IssueBook issueNewBook(IssueBookDto issueBookDto);

}

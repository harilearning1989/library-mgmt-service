package com.web.library.services.library;

import com.web.library.dtos.ContactDto;

import java.util.List;

public interface ContactService {
    List<ContactDto> findAll();

    ContactDto createContact(ContactDto contactDto);
}

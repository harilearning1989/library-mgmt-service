package com.web.library.services.library;

import com.web.library.dtos.IssueBookDto;
import com.web.library.models.library.IssueBook;
import com.web.library.models.library.ReturnBook;

import java.util.List;

public interface BookReturnService {
    List<ReturnBook> allReturnedBooks();

    ReturnBook returnOldBook(ReturnBook returnBook);
    IssueBook returnIssuedBook(IssueBookDto issueBookDto);
}

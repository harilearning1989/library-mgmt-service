package com.web.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryMgmtApplication {

	//https://www.springjavatutorial.com/2022/07/spring-rest-api-authentication-using-jwt.html
	//https://www.bezkoder.com/spring-boot-jwt-authentication/
	public static void main(String[] args) {
		SpringApplication.run(LibraryMgmtApplication.class, args);
	}

}

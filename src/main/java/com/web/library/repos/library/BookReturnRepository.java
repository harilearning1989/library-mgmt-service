package com.web.library.repos.library;

import com.web.library.models.library.ReturnBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookReturnRepository extends JpaRepository<ReturnBook, Integer> {

}

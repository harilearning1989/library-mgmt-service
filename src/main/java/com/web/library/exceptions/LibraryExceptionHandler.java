package com.web.library.exceptions;

import com.web.library.response.GlobalResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
public class LibraryExceptionHandler {

    @ExceptionHandler(value = ResponseStatusException.class)
    public ResponseEntity<GlobalResponse> exception(ResponseStatusException exception) {
        GlobalResponse globalResponse =
                GlobalResponse.builder()
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(exception.getReason())
                        .build();
        return new ResponseEntity<>(globalResponse,HttpStatus.NOT_FOUND);
    }
}

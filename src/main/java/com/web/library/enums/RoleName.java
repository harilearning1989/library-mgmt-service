package com.web.library.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN,
    ROLE_STUDENT,
    ROLE_STAFF
}

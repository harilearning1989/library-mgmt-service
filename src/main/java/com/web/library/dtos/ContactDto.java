package com.web.library.dtos;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ContactDto {

    private int id;
    private String name;
    private String email;
    private String mobile;
    private String message;

}
